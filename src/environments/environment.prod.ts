export const environment = {
  production: true,
  backendUrl: 'https://sheltered-cliffs-34052.herokuapp.com/api',
  cryptoApi: 'https://min-api.cryptocompare.com'
};
