export const environment = {
  production: false,
  backendUrl: 'https://sheltered-cliffs-34052.herokuapp.com/api',
  cryptoApi: 'https://min-api.cryptocompare.com'
}
