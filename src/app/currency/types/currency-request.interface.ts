export interface CurrencyRequestInterface {
    acronym: string
    name: string
}
