import {LinksInterface} from 'src/app/shared/types/links.interface'

export interface CurrencyInterface {
    id: number
    acronym: string
    name: string
}

