import {CurrencyInterface} from 'src/app/currency/types/currency.interface'

export interface CurrencyResponseInterface {
  currencies: CurrencyInterface
}
