import {NgModule} from '@angular/core'
import {Routes, RouterModule} from '@angular/router'
import {CurrencyComponent} from 'src/app/currency/components/currency/currency.component'
import {CreateCurrencyComponent} from 'src/app/currency/components/create-currency/create-currency.component'
import {EditCurrencyComponent} from 'src/app/currency/components/edit-currency/edit-currency.component'

const routes: Routes = [
  {path: 'currency', component: CurrencyComponent},
  {path: 'currency/new', component: CreateCurrencyComponent},
  {path: 'currency/:id/edit', component: EditCurrencyComponent},
]

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CurrencyRoutingModule {}
