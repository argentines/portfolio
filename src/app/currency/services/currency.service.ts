import {Injectable} from '@angular/core'
import {HttpClient} from '@angular/common/http'
import {environment} from 'src/environments/environment'
import {Observable} from 'rxjs'
import {map} from 'rxjs/operators'

import {CurrencyResponseInterface} from 'src/app/currency/types/currency-response.interface'
import {CurrencyRequestInterface} from 'src/app/currency/types/currency-request.interface'
import {CurrencyInterface} from 'src/app/currency/types/currency.interface'


@Injectable()
export class CurrencyService {

  constructor(private http: HttpClient) { }

  getAllCurrency():Observable<CurrencyResponseInterface[]>{
    return this.http.get<CurrencyResponseInterface[]>(`${environment.backendUrl}/currency`).pipe(
      map((embedded) => embedded['_embedded'].currencies)
    )
  }

  getCurrencyById(id: number): Observable<CurrencyInterface>{
    return this.http.get<CurrencyInterface>(`${environment.backendUrl}/currency/${id}`)
  }

  addCurrency(currency: CurrencyRequestInterface): Observable<CurrencyResponseInterface>{
    return this.http.post<CurrencyResponseInterface>(`${environment.backendUrl}/currency`, currency)
  }

  updateCurrency(id: string, currency: CurrencyInterface): Observable<CurrencyResponseInterface>{
    return this.http.put<CurrencyResponseInterface>(`${environment.backendUrl}/currency/${id}`, currency)
  }

  removeCurrency(id: number): void{
    this.http.delete(`${environment.backendUrl}/currency/${id}`)
  }
}
