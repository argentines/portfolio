import {NgModule} from '@angular/core'
import {CommonModule} from '@angular/common'
import {FontAwesomeModule} from '@fortawesome/angular-fontawesome'

import {CurrencyService} from 'src/app/currency/services/currency.service'

import {CurrencyComponent} from 'src/app/currency/components/currency/currency.component'
import {CreateCurrencyComponent} from 'src/app/currency/components/create-currency/create-currency.component'
import {EditCurrencyComponent} from 'src/app/currency/components/edit-currency/edit-currency.component'

import {CurrencyRoutingModule} from 'src/app/currency/currency-routing.module'
import {CurrencyFormModule} from 'src/app/shared/modules/currency-form/currency-form.module'


@NgModule({
  declarations: [CurrencyComponent, CreateCurrencyComponent, EditCurrencyComponent],
  imports: [CommonModule,
            CurrencyRoutingModule,
            CurrencyFormModule,
            FontAwesomeModule],
  exports: [CurrencyComponent],
  providers: [CurrencyService]

})
export class CurrencyModule { }
