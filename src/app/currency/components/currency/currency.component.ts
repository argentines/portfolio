import {Component, OnDestroy, OnInit} from '@angular/core';
import {CurrencyService} from 'src/app/currency/services/currency.service'
import {Subscription} from 'rxjs'

import {CurrencyResponseInterface} from 'src/app/currency/types/currency-response.interface'
import {faEdit, faTrashAlt, faPlus} from '@fortawesome/free-solid-svg-icons'
import {CryptoService} from 'src/app/shared/services/crypto.service'

@Component({
  selector: 'pf-currency',
  templateUrl: './currency.component.html',
  styleUrls: ['./currency.component.scss']
})
export class CurrencyComponent implements OnInit, OnDestroy {

  allCurrencySubscribe: Subscription
  oneCurrencySubscribe: Subscription
  currencyList: CurrencyResponseInterface[] | null
  crypto: any
  objectKeys = Object.keys

  constructor(private currencyService: CurrencyService, private cryptoService: CryptoService) { }
  new = faPlus
  trash = faTrashAlt
  edit = faEdit

  ngOnInit(): void {
    this.initializeValues()
    this.cryptoService.getPrices().subscribe((res) => {
      this.crypto = res
    })
  }

  ngOnDestroy(): void {
    if(this.allCurrencySubscribe) {
      this.allCurrencySubscribe.unsubscribe()
    }

    if(this.oneCurrencySubscribe) {
      this.oneCurrencySubscribe.unsubscribe()
    }
  }

  private initializeValues(): void {
    this.allCurrencySubscribe = this.currencyService.getAllCurrency().subscribe(
      currency => this.currencyList = currency
    )
  }

  findOneCurrency(id: number){
    this.oneCurrencySubscribe = this.currencyService.getCurrencyById(id).subscribe(console.log)
  }

  removeCurrency(id: number): void{
    this.currencyService.removeCurrency(id);
  }
}
