import {Component, OnInit} from '@angular/core'
import {map} from 'rxjs/operators'
import {Observable} from 'rxjs'
import {ActivatedRoute} from '@angular/router'

import {CurrencyService} from 'src/app/currency/services/currency.service'

import {CurrencyInterface} from 'src/app/currency/types/currency.interface'
import {CurrencyRequestInterface} from 'src/app/currency/types/currency-request.interface'


@Component({
  selector: 'pf-edit-currency',
  templateUrl: './edit-currency.component.html',
  styleUrls: ['./edit-currency.component.scss']
})
export class EditCurrencyComponent implements OnInit{

  initialValues: Observable<CurrencyRequestInterface>
  isSubmitting: boolean
  id: string

  constructor(private currencyService: CurrencyService, private route: ActivatedRoute) {}

  ngOnInit(): void {
    this.id = this.route.snapshot.paramMap.get('id')
    this.initialValues = this.route.queryParams.pipe(
      map((currency) => {
          return {
            name: currency.name,
            acronym: currency.acronym
          }
        }
      )
    )
  }

  onSubmit(currency: CurrencyInterface): void {
    this.currencyService.updateCurrency(this.id, currency)
  }
}
