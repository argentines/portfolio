import {Component, OnInit} from '@angular/core';
import {CurrencyRequestInterface} from 'src/app/currency/types/currency-request.interface'
import {CurrencyService} from 'src/app/currency/services/currency.service'
import {CryptoService} from 'src/app/shared/services/crypto.service'

@Component({
  selector: 'pf-create-currency',
  templateUrl: './create-currency.component.html',
  styleUrls: ['./create-currency.component.scss']
})
export class CreateCurrencyComponent implements OnInit{

  initialValues: CurrencyRequestInterface = {
    acronym: '',
    name: ''
  }
  isSubmitting: boolean
  crypto: any
  objectKeys = Object.keys

  constructor(private currencyService: CurrencyService, private cryptoService: CryptoService) {}

  ngOnInit(): void {
    this.cryptoService.getPrices().subscribe((res) => {
      this.crypto = res
    })
  }

  onSubmit(currency: CurrencyRequestInterface): void {
    this.currencyService.addCurrency(currency)
  }

}
