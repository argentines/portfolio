import {PortfolioInterface} from 'src/app/portfolio/types/portfolio.interface'

export interface PortfolioResponseInterface {
    portfolios: PortfolioInterface
}
