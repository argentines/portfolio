import {Component} from '@angular/core'
import {PortfolioInputInterface} from 'src/app/portfolio/types/portfolio-input.interface'
import {PortfolioService} from 'src/app/portfolio/services/portfolio.service'

@Component({
  selector: 'pf-create-portfolio',
  templateUrl: './create-portfolio.component.html',
  styleUrls: ['./create-portfolio.component.scss']
})
export class CreatePortfolioComponent {

  initialValues: PortfolioInputInterface = {
    name: ''
  }
  isSubmitting: boolean

  constructor(private portfolioService: PortfolioService) {}

  onSubmit(portfolio: PortfolioInputInterface): void {
    this.portfolioService.addPortfolio(portfolio)
  }
}
