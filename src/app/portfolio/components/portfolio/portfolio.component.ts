import {Component, OnInit} from '@angular/core'
import {PortfolioService} from 'src/app/portfolio/services/portfolio.service'
import {Subscription} from 'rxjs'

import {PortfolioInterface} from 'src/app/portfolio/types/portfolio.interface'
import {faEdit, faPlus, faTrashAlt} from '@fortawesome/free-solid-svg-icons'
import {PortfolioResponseInterface} from '../../types/portfolio-response.interface';

@Component({
  selector: 'pf-portfolio',
  templateUrl: './portfolio.component.html',
  styleUrls: ['./portfolio.component.scss']
})
export class PortfolioComponent implements OnInit {

  allPortfolioSubscribe: Subscription
  onePortfolioSubscribe: Subscription
  portfolioList: PortfolioResponseInterface[]

  constructor(private portfolioService: PortfolioService) { }

  new = faPlus
  trash = faTrashAlt
  edit = faEdit

  ngOnInit(): void {
    this.initializeValues()
  }

  ngOnDestroy(): void {
    if(this.allPortfolioSubscribe) {
      this.allPortfolioSubscribe.unsubscribe()
    }

    if(this.onePortfolioSubscribe) {
      this.onePortfolioSubscribe.unsubscribe()
    }
  }

  private initializeValues(): void {
    this.allPortfolioSubscribe = this.portfolioService.getAllPortfolio().subscribe(
      portfolios => {this.portfolioList = portfolios}
    )
  }

  findOnePortfolio(id: number){
    this.onePortfolioSubscribe = this.portfolioService.getPortfolioById(id).subscribe(console.log)
  }

  removePortfolio(id: number): void{
    this.portfolioService.removePortfolio(id)
  }

}
