import {Component, OnInit} from '@angular/core'
import {map} from 'rxjs/operators'
import {Observable} from 'rxjs'
import {ActivatedRoute} from '@angular/router'

import {PortfolioInputInterface} from 'src/app/portfolio/types/portfolio-input.interface'

import {PortfolioService} from 'src/app/portfolio/services/portfolio.service'

@Component({
  selector: 'pf-edit-portfolio',
  templateUrl: './edit-portfolio.component.html',
  styleUrls: ['./edit-portfolio.component.scss']
})
export class EditPortfolioComponent implements OnInit{

  initialValues: Observable<PortfolioInputInterface>
  isSubmitting: boolean
  id: number

  constructor(private portfolioService: PortfolioService, private route: ActivatedRoute) {}

  ngOnInit(): void {
    this.id = +this.route.snapshot.paramMap.get('id')
    this.initialValues = this.route.queryParams.pipe(
      map((portfolio) => {
          return {
            name: portfolio.name
          }
        }
      )
    )
  }

  onSubmit(portfolio: PortfolioInputInterface): void {
    this.portfolioService.updatePortfolio(this.id, portfolio)
  }
}
