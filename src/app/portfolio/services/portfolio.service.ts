import {Injectable} from '@angular/core'
import {HttpClient} from '@angular/common/http'
import {map} from 'rxjs/operators'
import {Observable} from 'rxjs'

import {environment} from 'src/environments/environment'
import {PortfolioResponseInterface} from 'src/app/portfolio/types/portfolio-response.interface'
import {PortfolioInputInterface} from 'src/app/portfolio/types/portfolio-input.interface'

@Injectable()
export class PortfolioService {

  constructor(private http: HttpClient) { }

  getAllPortfolio():Observable<PortfolioResponseInterface[]>{
    return this.http.get<PortfolioResponseInterface[]>(`${environment.backendUrl}/portfolio`).pipe(
      map((embedded) => embedded['_embedded'].portfolios)
    )
  }

  getPortfolioById(id: number):Observable<PortfolioResponseInterface>{
    return this.http.get<PortfolioResponseInterface>(`${environment.backendUrl}/portfolio/${id}`)
  }

  addPortfolio(portfolio: PortfolioInputInterface): Observable<PortfolioResponseInterface>{
    return this.http.post<PortfolioResponseInterface>(`${environment.backendUrl}/portfolio`, portfolio)
  }

  updatePortfolio(id: number, portfolio: PortfolioInputInterface): Observable<PortfolioResponseInterface>{
    return this.http.put<PortfolioResponseInterface>(`${environment.backendUrl}/portfolio/${id}`, portfolio)
  }

  removePortfolio(id: number): void{
    this.http.delete(`${environment.backendUrl}/portfolio/${id}`)
  }
}
