import {NgModule} from '@angular/core'
import {CommonModule} from '@angular/common'
import {PortfolioRoutingModule} from 'src/app/portfolio/portfolio-routing.module'
import {FontAwesomeModule} from '@fortawesome/angular-fontawesome'
import {RouterModule} from '@angular/router'

import {PortfolioService} from 'src/app/portfolio/services/portfolio.service'

import {PortfolioFormModule} from 'src/app/shared/modules/portfolio-form/portfolio-form.module'

import {PortfolioComponent} from 'src/app/portfolio/components/portfolio/portfolio.component'
import {CreatePortfolioComponent} from 'src/app/portfolio/components/create-partfolio/create-portfolio.component'
import {EditPortfolioComponent} from 'src/app/portfolio/components/edit-porfolio/edit-portfolio.component'

@NgModule({
  declarations: [PortfolioComponent, CreatePortfolioComponent, EditPortfolioComponent],
  exports: [PortfolioComponent],
  imports: [CommonModule,
            RouterModule,
            PortfolioRoutingModule,
            FontAwesomeModule,
            PortfolioFormModule],
  providers: [PortfolioService]
})
export class PortfolioModule { }
