import {NgModule} from '@angular/core'
import {Routes, RouterModule} from '@angular/router'

import {CreatePortfolioComponent} from 'src/app/portfolio/components/create-partfolio/create-portfolio.component'
import {PortfolioComponent} from 'src/app/portfolio/components/portfolio/portfolio.component'
import {EditPortfolioComponent} from 'src/app/portfolio/components/edit-porfolio/edit-portfolio.component'

const routes: Routes = [
  {path: 'portfolio', component: PortfolioComponent},
  {path: 'portfolio/new', component: CreatePortfolioComponent},
  {path: 'portfolio/:id/edit', component: EditPortfolioComponent}
]

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PortfolioRoutingModule {}
