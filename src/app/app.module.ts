import {BrowserModule} from '@angular/platform-browser'
import {NgModule} from '@angular/core'
import {HttpClientModule} from '@angular/common/http'
import {AppRoutingModule} from './app-routing.module'
import {RouterModule} from '@angular/router'

import {AppComponent} from 'src/app/app.component'

import {CurrencyModule} from 'src/app/currency/currency.module'
import {TopBarModule} from 'src/app/shared/modules/top-bar/top-bar.module'
import {PortfolioModule} from 'src/app/portfolio/portfolio.module'
import {PortfolioLinesModule} from 'src/app/portfolio-lines/portfolio-lines.module'
import {CryptoService} from './shared/services/crypto.service';


@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    CurrencyModule,
    HttpClientModule,
    RouterModule,
    TopBarModule,
    PortfolioModule,
    PortfolioLinesModule,
    AppRoutingModule
  ],
  providers: [CryptoService],
  bootstrap: [AppComponent]
})
export class AppModule { }
