import {Component, OnInit} from '@angular/core'
import {map} from 'rxjs/operators'
import {Observable} from 'rxjs'
import {ActivatedRoute} from '@angular/router'

import {PortfolioLinesService} from 'src/app/portfolio-lines/services/portfolio-lines.service'
import {PortfolioLinesInterface} from 'src/app/portfolio-lines/types/portfolio-lines.interface'

@Component({
  selector: 'pf-edit-portfolio-lines',
  templateUrl: './edit-portfolio-lines.component.html',
  styleUrls: ['./edit-portfolio-lines.component.scss']
})
export class EditPortfolioLinesComponent implements OnInit{

  initialValues: Observable<any>
  isSubmitting: boolean
  id: number

  constructor(private portfolioLinesService: PortfolioLinesService, private route: ActivatedRoute) {}

  ngOnInit(): void {
    this.id = +this.route.snapshot.paramMap.get('id')
    this.initialValues = this.route.queryParams.pipe(
      map((portfolioLine) => {
          return {
            amount: portfolioLine.amount
          }
        }
      )
    )
  }

  onSubmit(portfolio: PortfolioLinesInterface): void {
    this.portfolioLinesService.updatePortfolioLine(this.id, portfolio)
  }
}
