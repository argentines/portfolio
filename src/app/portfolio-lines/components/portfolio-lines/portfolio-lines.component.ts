import { Component, OnInit } from '@angular/core'
import {Subscription} from 'rxjs'
import {PortfolioLinesService} from 'src/app/portfolio-lines/services/portfolio-lines.service'
import {faEdit, faPlus, faTrashAlt} from '@fortawesome/free-solid-svg-icons'

@Component({
  selector: 'pf-portfolio-lines',
  templateUrl: './portfolio-lines.component.html',
  styleUrls: ['./portfolio-lines.component.scss']
})
export class PortfolioLinesComponent implements OnInit {

  new = faPlus
  trash = faTrashAlt
  edit = faEdit

  allPortfolioSubscribe: Subscription
  onePortfolioSubscribe: Subscription
  portfolioLineList: any

  constructor(private portfolioLinesService: PortfolioLinesService) { }

  ngOnInit(): void {
    this.initializeValues()
  }

  ngOnDestroy(): void {
    if(this.allPortfolioSubscribe) {
      this.allPortfolioSubscribe.unsubscribe()
    }

    if(this.onePortfolioSubscribe) {
      this.onePortfolioSubscribe.unsubscribe()
    }
  }

  private initializeValues(): void {
    this.allPortfolioSubscribe = this.portfolioLinesService.getAllPortfolioLine().subscribe(
      portfolios => {this.portfolioLineList = portfolios}
    )
  }

  findOnePortfolioLine(id: number){
    this.onePortfolioSubscribe = this.portfolioLinesService.getPortfolioLineById(id).subscribe(console.log)
  }

  removePortfolioLine(id: number): void{
    this.portfolioLinesService.removePortfolioLine(id)
  }

}
