import {Component} from '@angular/core'
import {PortfolioLinesInterface} from 'src/app/portfolio-lines/types/portfolio-lines.interface'

import {PortfolioLinesService} from 'src/app/portfolio-lines/services/portfolio-lines.service'

@Component({
  selector: 'pf-create-portfolio-lines',
  templateUrl: './create-portfolio-lines.component.html',
  styleUrls: ['./create-portfolio-lines.component.scss']
})
export class CreatePortfolioLinesComponent {

  initialValues = {
    amount: ''
  }
  isSubmitting: boolean

  constructor(private portfolioLinesService: PortfolioLinesService) {}

  onSubmit(portfolio: PortfolioLinesInterface): void {
    this.portfolioLinesService.addPortfolioLine(portfolio)
  }
}
