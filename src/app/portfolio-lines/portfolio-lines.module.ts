import {NgModule} from '@angular/core'
import {CommonModule} from '@angular/common'
import {RouterModule} from '@angular/router'
import {FontAwesomeModule} from '@fortawesome/angular-fontawesome'

import {PortfolioLinesService} from 'src/app/portfolio-lines/services/portfolio-lines.service'

import {PortfolioLineRoutingModule} from 'src/app/portfolio-lines/portfolio-line-routing.module'
import {PortfolioLineFormModule} from 'src/app/shared/modules/portfolio-lines-form/portfolio-line-form.module'

import {PortfolioLinesComponent} from 'src/app/portfolio-lines/components/portfolio-lines/portfolio-lines.component'
import {CreatePortfolioLinesComponent} from 'src/app/portfolio-lines/components/create-partfolio-lines/create-portfolio-lines.component'
import {EditPortfolioLinesComponent} from 'src/app/portfolio-lines/components/edit-porfolio-lines/edit-portfolio-lines.component';
import {PortfolioFormModule} from '../shared/modules/portfolio-form/portfolio-form.module';

@NgModule({
  declarations: [PortfolioLinesComponent, CreatePortfolioLinesComponent, EditPortfolioLinesComponent],
  exports: [],
  imports: [CommonModule,
    FontAwesomeModule,
    RouterModule,
    PortfolioLineRoutingModule,
    PortfolioLineFormModule,
    PortfolioFormModule],
  providers: [PortfolioLinesService]
})
export class PortfolioLinesModule { }
