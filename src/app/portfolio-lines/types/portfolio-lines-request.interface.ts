export interface PortfolioLinesRequestInterface {
  amount: number
  currency: string
  portfolio: string
}
