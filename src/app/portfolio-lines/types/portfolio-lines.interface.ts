export interface PortfolioLinesInterface {
    amount: number
    currency: string
    portfolio: string
}
