import {PortfolioLinesInterface} from 'src/app/portfolio-lines/types/portfolio-lines.interface'

export interface PortfolioLinesResponseInterface {
  portfolioLines: PortfolioLinesInterface
}
