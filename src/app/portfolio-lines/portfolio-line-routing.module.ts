import {NgModule} from '@angular/core'
import {Routes, RouterModule} from '@angular/router'

import {PortfolioLinesComponent} from 'src/app/portfolio-lines/components/portfolio-lines/portfolio-lines.component';
import {CreatePortfolioLinesComponent} from 'src/app/portfolio-lines/components/create-partfolio-lines/create-portfolio-lines.component';
import {EditPortfolioLinesComponent} from 'src/app/portfolio-lines/components/edit-porfolio-lines/edit-portfolio-lines.component';

const routes: Routes = [
  {path: 'portfolio-line', component: PortfolioLinesComponent},
  {path: 'portfolio-line/new', component: CreatePortfolioLinesComponent},
  {path: 'portfolio-line/:id/edit', component: EditPortfolioLinesComponent}
]

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PortfolioLineRoutingModule {}
