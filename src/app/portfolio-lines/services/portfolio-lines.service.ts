import {Injectable} from '@angular/core'
import {HttpClient} from '@angular/common/http'
import {environment} from 'src/environments/environment'
import {Observable} from 'rxjs'
import {map} from 'rxjs/operators'

import {PortfolioLinesResponseInterface} from 'src/app/portfolio-lines/types/portfolio-lines-response.interface'
import {PortfolioLinesInterface} from 'src/app/portfolio-lines/types/portfolio-lines.interface'

@Injectable()
export class PortfolioLinesService {

  constructor(private http: HttpClient) { }

  getAllPortfolioLine():Observable<PortfolioLinesResponseInterface[]>{
    return this.http.get<PortfolioLinesResponseInterface[]>(`${environment.backendUrl}/portfolio/1/lines`).pipe(
      map((embedded) => embedded['_embedded'].portfolioLines)
    )
  }

  getPortfolioLineById(id: number):Observable<PortfolioLinesResponseInterface>{
    return this.http.get<PortfolioLinesResponseInterface>(`${environment.backendUrl}/portfolioline/${id}`)
  }

  addPortfolioLine(portfolio: PortfolioLinesInterface):Observable<PortfolioLinesResponseInterface>{
    return this.http.post<PortfolioLinesResponseInterface>(`${environment.backendUrl}/portfolioline`, portfolio)
  }

  updatePortfolioLine(id: number, portfolio: PortfolioLinesInterface):Observable<PortfolioLinesResponseInterface>{
    return this.http.put<any>(`${environment.backendUrl}/portfolioline/${id}`, portfolio)
  }

  removePortfolioLine(id: number): void{
    this.http.delete(`${environment.backendUrl}/portfolioline/${id}`)
  }
}
