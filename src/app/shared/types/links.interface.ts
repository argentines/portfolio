export interface LinksInterface {
    href: string
    rel: string
    templated: boolean
}
