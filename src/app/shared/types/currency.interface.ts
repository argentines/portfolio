export interface CurrencyInterface {
    id: number
    acronym: string
    name: string
}
