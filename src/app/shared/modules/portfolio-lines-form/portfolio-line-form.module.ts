import {NgModule} from '@angular/core'
import {CommonModule} from '@angular/common'
import {ReactiveFormsModule} from '@angular/forms'

import {PortfolioLinesFormComponent} from 'src/app/shared/modules/portfolio-lines-form/components/portfolio-lines-form/portfolio-lines-form.component'

@NgModule({
  imports: [CommonModule,
            ReactiveFormsModule],
  exports: [PortfolioLinesFormComponent],
  declarations: [PortfolioLinesFormComponent]
})

export class PortfolioLineFormModule {}
