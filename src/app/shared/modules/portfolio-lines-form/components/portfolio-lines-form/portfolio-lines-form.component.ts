import {Component, Input, OnInit, Output, EventEmitter} from '@angular/core'
import {FormBuilder, FormGroup} from '@angular/forms'

@Component({
  selector: 'pf-portfolio-lines-form',
  templateUrl: './portfolio-lines-form.component.html',
  styleUrls: ['./portfolio-lines-form.component.scss']
})
export class PortfolioLinesFormComponent implements OnInit {

  @Input('initialValue') initialValueProps: any
  @Input('isSubmitting') isSubmittingProps: boolean

  @Output('portfolioLineSubmit') currencySubmitEvent = new EventEmitter<any>()

  form: FormGroup

  constructor(private fb: FormBuilder) {}

  ngOnInit() {
    this.initializeForm()
  }

  initializeForm(): void {
    this.form = this.fb.group({
      amount: this.initialValueProps.amount,
      currency: this.initialValueProps.currency,
      portfolio: this.initialValueProps.portfolio
    })
  }

  onSubmit(): void {
    this.currencySubmitEvent.emit(this.form.value)
  }
}
