import {NgModule} from '@angular/core'
import {CommonModule} from '@angular/common'
import {ReactiveFormsModule} from '@angular/forms'

import {PortfolioFormComponent} from 'src/app/shared/modules/portfolio-form/components/portfolio-form/portfolio-form.component'

@NgModule({
  imports: [CommonModule,ReactiveFormsModule],
  exports: [PortfolioFormComponent],
  declarations: [PortfolioFormComponent]
})

export class PortfolioFormModule {}
