import {Component, Input, OnInit, Output, EventEmitter} from '@angular/core'
import {FormBuilder, FormGroup} from '@angular/forms'
import {PortfolioInputInterface} from 'src/app/portfolio/types/portfolio-input.interface'

@Component({
  selector: 'pf-portfolio-form',
  templateUrl: './portfolio-form.component.html',
  styleUrls: ['./portfolio-form.component.scss']
})
export class PortfolioFormComponent implements OnInit {

  @Input('initialValue') initialValueProps: PortfolioInputInterface
  @Input('isSubmitting') isSubmittingProps: boolean

  @Output('portfolioSubmit') portfolioSubmitEvent = new EventEmitter<PortfolioInputInterface>()

  form: FormGroup

  constructor(private fb: FormBuilder) {}

  ngOnInit() {
    this.initializeForm()
  }

  initializeForm(): void {
    this.form = this.fb.group({
      name: this.initialValueProps.name
    })
  }

  onSubmit(): void {
    this.portfolioSubmitEvent.emit(this.form.value)
  }
}
