import {NgModule} from '@angular/core'
import {CommonModule} from '@angular/common'
import {ReactiveFormsModule} from '@angular/forms'

import {CurrencyFormComponent} from 'src/app/shared/modules/currency-form/components/currency-form/currency-form.component'

@NgModule({
  imports: [CommonModule,
            ReactiveFormsModule],
  exports: [CurrencyFormComponent],
  declarations: [CurrencyFormComponent]
})

export class CurrencyFormModule {}
