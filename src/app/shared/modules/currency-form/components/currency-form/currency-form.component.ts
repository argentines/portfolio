import {Component, Input, OnInit, Output, EventEmitter} from '@angular/core'
import {FormBuilder, FormGroup} from '@angular/forms'
import {CurrencyRequestInterface} from 'src/app/currency/types/currency-request.interface'

@Component({
  selector: 'pf-currency-form',
  templateUrl: './currency-form.component.html',
  styleUrls: ['./currency-form.component.scss']
})
export class CurrencyFormComponent implements OnInit {

  @Input('initialValue') initialValueProps: CurrencyRequestInterface //CurrencyRequestInterface
  @Input('isSubmitting') isSubmittingProps: boolean

  @Output('currencySubmit') currencySubmitEvent = new EventEmitter<CurrencyRequestInterface>()

  form: FormGroup

  constructor(private fb: FormBuilder) {}

  ngOnInit() {
    this.initializeForm()
  }

  initializeForm(): void {
    this.form = this.fb.group({
      acronym: this.initialValueProps.acronym,
      name: this.initialValueProps.name
    })
  }

  onSubmit(): void {
    this.currencySubmitEvent.emit(this.form.value)
  }
}
