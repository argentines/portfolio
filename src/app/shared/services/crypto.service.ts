import {Injectable} from '@angular/core'
import {HttpClient} from '@angular/common/http'
import {environment} from 'src/environments/environment'
import {map} from 'rxjs/operators'

@Injectable()
export class CryptoService {

  crypto: any

  constructor(private http: HttpClient) { }

  getPrices() {
    return this.http.get(`${environment.cryptoApi}/data/price?fsym=BTC&tsyms=EUR`)
      .pipe(
        map(req => this.crypto = req)
      )
  }
}
